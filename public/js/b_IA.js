/**
 * @author MOR    Nicolas
 * @author NGUYEN Nicolas
 */

const brain    = require('brain.js');
const fs       = require('fs');
const readline = require('readline'); 
const file     = "preTrain.json";
const DEBUG    = true 

var net = new brain.NeuralNetwork();

exports.readingFile = function (file){
    //Reading the file.
    if (fs.existsSync(file))
    {
        console.log("Reading...");
        let IAdata = fs.readFileSync(file);
        console.log("Readed.");
        if (IAdata.length == 0)
        {
            console.log("File : Empty file.");
            console.log("File : Creating file %s", file);
        }
        else 
        {
            console.log("File : Loading the neural network...");
            net.fromJSON(JSON.parse(IAdata));
            console.log("File : Neural Network is loaded.");
        }
    }
}

exports.testIASantorini = function ()
{
    let lastPosData = [30,10,21,20,10,20,40,21,0,0,30,30,30,22,20,40,40,30,20,20,10,20,30,22,20];
    let nextPosData = [30,10,21,20,10,20,40,21,0,0,30,30,30,20,20,40,40,32,20,20,10,30,30,22,20];

    console.log(net.run(lastPosData.concat(nextPosData).map(e => Number(e)/100)));
    console.log(net.run([1,20,0,10,10,20,10,10,20,0,40,0,40,0,1,10,12,10,10,10,0,12,0,20,10,1,20,0,10,10,20,10,10,20,0,40,0,40,0,1,12,12,10,10,10,0,20,0,20,10].map(e => Number(e)/100)));
    console.log(net.run([10,0,0,0,0,10,1,0,0,0,10,1,0,20,0,2,0,0,12,10,10,0,0,0,10,10,0,0,0,0,10,1,0,0,0,10,1,0,20,0,2,0,0,10,10,10,0,2,10,10].map(e => Number(e)/100)));
}

exports.readFormatedDataSync = function(file)
{
    if (fs.existsSync(file))
    {
        let winData     = undefined;
        let lastPosData = undefined;
        let nextPosData = undefined;

        console.log("Reading...");
        let IAdata = fs.readFileSync(file, 'utf8').split('\n');
        console.log("Readed.");
        if (IAdata.length == 0)
        {
            console.log("File : Empty file.");
            console.log("File : Creating file %s", file);
        }
        else 
        {
            //Separe all line --> \n
            let array = [];
            array.push(...IAdata);

            
            //For all line.
            for(let i = 0; i < array.length; ++i)
            {
                let tmpArray = IAdata[i].split(";");
                if(tmpArray == ''){
                    break;
                }
                winData     = tmpArray[0];
                lastPosData = tmpArray[1].split(',');
                nextPosData = tmpArray[2].split(',');

                //Normalize data.
                for( let i = 0; i < lastPosData.length; ++i)
                {
                    lastPosData[i] = parseInt(lastPosData[i], 10)/100;
                }

                for( let i = 0; i < nextPosData.length; ++i)
                {
                    nextPosData[i] = parseInt(nextPosData[i], 10)/100;
                }
                
                let tmp = 
                {
                    input  : lastPosData.concat(nextPosData),
                    output : [winData]
                };

                console.log(tmp);
                console.log("Training...");
                net.train(tmp);
                console.log("End of train");
            }

        }
        console.log("Finish");
        console.log("Exporting I.A...");
        let json = net.toJSON();
        fs.writeFile("./preTrain.json", JSON.stringify(json, null, 4), (err) => {
            if (err) 
            {
                console.error(err);
                return;
            };
            console.log("File has been created");
        });
    }
}


exports.testIA = function (lastPosData, nextPosData)
{
    
    console.log(net.run(lastPosData.concat(nextPosData).map(e => Number(e)/100)));
    
    let json = net.toJSON();
    fs.writeFile("./preTrain.ia", JSON.stringify(json, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log("File has been created");
    });
}



exports.net  = net;
exports.file = file;

