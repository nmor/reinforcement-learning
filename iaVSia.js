/**
 * This file is only for traing I.A
 */

"use strict";
var fs       = require('fs');
var readline = require("readline");
const brain  = require('brain.js');


var selectedPions = undefined;
var currentPlayer = undefined;
var readFile      = 1;
var arrayReadIA   = [];

var net = new brain.NeuralNetwork();

/**let rawdata = fs.readFileSync('preTrain.json');
if (rawdata != undefined || rawdata != null){
    net.fromJSON(JSON.parse(rawdata));
}*/
readFormatedData();


/***************************************************************************
 *                                                                         *
 *                                 IA                                      *
 *                                                                         *
 ***************************************************************************/
  



function trainIA(){
    console.log("Starting I.A");
    let lastPosData = [0,0,21,10,0,0,30,0,10,10,12,1,10,20,10,20,10,20,2,0,0,10,20,10,0];
    let nextPosData = [0,10,20,10,0,0,31,0,10,10,12,1,10,20,10,20,10,20,2,0,0,10,20,10,0];

    net.train(tmp);
    console.log("Ending I.A");
}

function testIA(){
    console.log("TEST I.A");
    
    let lastPosData = [30,10,21,20,10,20,40,21,0,0,30,30,30,22,20,40,40,30,20,20,10,20,30,22,20];
    let nextPosData = [30,10,21,20,10,20,40,21,0,0,30,30,30,20,20,40,40,32,20,20,10,30,30,22,20];

    console.log(net.run(lastPosData.concat(nextPosData).map(e => Number(e)/100)));
    console.log(net.run([1,20,0,10,10,20,10,10,20,0,40,0,40,0,1,10,12,10,10,10,0,12,0,20,10,1,20,0,10,10,20,10,10,20,0,40,0,40,0,1,12,12,10,10,10,0,20,0,20,10].map(e => Number(e)/100)));

    let json = net.toJSON();
    fs.writeFile("./preTrain.json", JSON.stringify(json, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log("File has been created");
    });
    //console.log(net.run([Array(50).fill("0", 0, 49)]));
    
}



/***************************************************************************
 *                                                                         *
 *                               CLASS                                     *
 *                                                                         *
 ***************************************************************************/


/**
 * Write and Read with this ressources.
 */
class File {

    constructor(nameOfFile){
        this.nameOfFile = nameOfFile;

        fs.appendFile('./test.data', 'HelloWorld', function(err){
            if(err) return console.error(err);
        });
    }

    write(data){
        fs.appendFile(this.nameOfFile, data, function(err){
            if(err) return console.error(err);
        });
    }
}


/**
 * Represent a Player.
 */
class Player{

    constructor(namePlayer, pions1, pions2){
        this.player = namePlayer;
        this.pions = [pions1, pions2];
    }

    getPions(x){
        switch(x){
            case 1 :
                return this.pions[0];
            case 2 :
                return this.pions[1];
            default :
                undefined;
        }
    }
}

/**
 * Represent a Building.
 */
class Building{

    constructor(){
        this.maxHeight = 4;
        this.floor     = 0;
    }

    canIncreaseBuilding(){
        if (this.maxHeight == this.floor){
            return false;
        }
        return true;
    }

    increaseBuilding(){
        if (this.maxHeight == this.floor){
            return false;
        }
        this.floor++;
        return true;
    }

    getNumberFloor(){
        return this.floor;
    }
}

class Pion {

    constructor(posX, posY){
        this.x = posX;
        this.y = posY;
    }

    moveTo(posX, posY) {
        this.x = posX;
        this.y = posY;
    }

    getPos(){
        return [this.x, this.y];
    }
}

/**
 * Represent one Cell in Array.
 */
class Cell{
    constructor(){
        this.building = new Building();
        //This would be a pawn.
    }


    build(){
        return this.building.increaseBuilding();
    }

    canMove(){
        if(this.building.getNumberFloor() == MAX_FLOOR){
            return true;
        }else {
            return false;
        }
    }

    getNumberFloor(){
        return this.building.getNumberFloor();
    }
}
class Game {
    constructor(){
        let x = MAX_COLONNE;
        let y = MAX_LIGNE;

        this.tmp = 0;

        this.game = new Array(MAX_LIGNE);
        for(let i = 0; i < MAX_COLONNE; ++i){
            this.game[i] = new Array(MAX_COLONNE);
        }

        for (let i = 0; i < x; ++i){
            for(let j = 0; j < y; ++j){
                this.game[i][j] = new Cell([0,0,0,0]);
            }
        }
        //keywordNICOLASPION
        let pions1Player1 = new Pion(0,0);
        let pions2Player1 = new Pion(0,4);
        let pions1Player2 = new Pion(4,4);
        let pions2Player2 = new Pion(4,0);


        this.player1 = new Player("I.A Random 1", pions1Player1, pions2Player1);
        this.player2 = new Player("I.A Random 2", pions1Player2, pions2Player2);
    }

    /**
     * Return the case of x and y
     * @param {*} x Row
     * @param {*} y
     */
    getGame(x,y){
        return this.game[x][y];
    }

    /**
     *
     * @param {*} x
     * @param {*} y
     * @param {*} value
     */
    setGame(x,y, value){
        this.game[x][y] = value;
    }

    /**
     * Return null if game can continue otherwise return the winner (1 or 2)
     */
    isGameIsFinish(){

        //Player 1 is block.
        if (!this.checkValidityOfMove(1,1) && !this.checkValidityOfMove(1,2)){
            return 2;
        }

        //Player 2 is block.
        else if (!this.checkValidityOfMove(2,1) && !this.checkValidityOfMove(2,2)){
            return 1;
        }

        //Player 1 is in max floor.
        let posPions1 = game.getPionOfPlayer(1,1).getPos();
        let posPions2 = game.getPionOfPlayer(1,2).getPos();

        if (this.getGame(posPions1[0], posPions1[1]).getNumberFloor() == MAX_FLOOR-1 || this.getGame(posPions2[0], posPions2[1]).getNumberFloor() == MAX_FLOOR-1){
            return 1;
        }

        //Player 2 is in max floor.
        posPions1 = game.getPionOfPlayer(2,1).getPos();
        posPions2 = game.getPionOfPlayer(2,2).getPos();
        if (this.getGame(posPions1[0], posPions1[1]).getNumberFloor() == MAX_FLOOR-1 || this.getGame(posPions2[0], posPions2[1]).getNumberFloor() == MAX_FLOOR-1){
            return 2;
        }
        return null;
    }


    /**
     * Get the player
     * @param {*} x
     */
    getPlayer(player){
        switch (player){
            case 1 :
                return this.player1;
            case 2 :
                return this.player2;
            default :
                return undefined;
        }
    }

    pionIsInCell(x,y){
        let arrayPions = [];

        arrayPions.push(this.getPionOfPlayer(1,1).getPos());
        arrayPions.push(this.getPionOfPlayer(1,2).getPos());
        arrayPions.push(this.getPionOfPlayer(2,1).getPos());
        arrayPions.push(this.getPionOfPlayer(2,2).getPos());

        console.log("\t\tpionIsInCell");
        console.log(arrayPions);

        for (let i = 0; i < 4; ++i){
            let pos = arrayPions[i];
            if(pos[0] == x && pos[1] == y){
                return true;
            }
        }
        return false;
    }

    getPionOfPlayer(player, pions){
        let playerLocal = this.getPlayer(player);
        if(playerLocal == undefined){
            return;
        }
        switch (pions){
            case 1 :
                return playerLocal.getPions(1);
            case 2 :
                return playerLocal.getPions(2);
            default :
                undefined;
        }
    }
    isBuildingFloorIsCorrect(currentX, currentY, destX, destY){
        console.log("pionsInCell = " + [destX,destY]+"end");

        console.log("\tCheck "+ this.pionIsInCell(destX,destY));
        if(destX<0 || destX>MAX_LIGNE-1 || destY<0 || destY>MAX_COLONNE-1){
            return false;
        }
        if (this.pionIsInCell(destX,destY)){
            console.log("\tFirst condition");
            return false;
        }
        if(currentX==destX && currentY==destY){
            console.log("\tSecond condition");
            return false;
        }
        if(-1 <= destX-currentX && destX-currentX <= 1 && -1 <= destY-currentY && destY-currentY <= 1){
            console.log("\tVictim");
            return true;
        }

        return false;
    }

    canIncreaseBuilding(x,y,player,pions){
        let pos =game.getPionOfPlayer(player,pions).getPos();

        return this.isBuildingFloorIsCorrect(pos[0],pos[1],x,y);
    };


    increaseBuilding(x, y,player,pions){
        if(!this.canIncreaseBuilding(x,y,player,pions)){
            return false;
        }
        return this.game[x][y].build();
    }

    movePionsOfPlayer(player, pions,x,y){
        let pos = game.getPionOfPlayer(player, pions).getPos();
        let currentX =  pos[0];
        let currentY =  pos[1];

        let floorStart = game.getGame(currentX,currentY).getNumberFloor();

        if(-1 <= x-currentX && x-currentX <= 1 && -1 <= y-currentY && y-currentY <= 1){
            if (game.checkValidityOfNewMove(player, pions, floorStart, x,y)){
                game.getPionOfPlayer(player, pions).moveTo(x,y);
                return true;
            }
        }
        else{
            console.log("Hello");
            return false;
        }
    }

    /**
     *
     * @param {int} player         : 1->player1 2->player2
     * @param {int} pions          : 1->pions1  2->pions2
     * @param {int} floorStart     : stating floor
     * @param {int} x              : new X cord.
     * @param {int} y              : new Y cord.
     */
    checkValidityOfNewMove(player, pions, floorStart, x, y){
        this.tmp++;

        if (this.tmp > 1000){
            
            console.log(printGame(game));
            console.log( game.getPlayer(1));
            console.log( game.getPlayer(2));

            console.log("pions : " + selectedPions );
            console.log ("player : " + currentPlayer );
            console.log(sendBoard(game));
            throw "Bad";
        }

        if(x < 0 || y < 0 || x > MAX_LIGNE-1|| y > MAX_COLONNE-1){
            console.log("Max ligne|max colonne\n");
            return false;
        }

        if (this.getGame(x,y).getNumberFloor() == MAX_FLOOR){
            console.log("Max Floor\n");
            return false;
        }

        if (this.getGame(x,y).getNumberFloor() > floorStart+1){
            console.log("Can(t up to +2.\n");
            return false;
        }

        let checkPos = game.getPionOfPlayer(1,1).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            console.log("Not same pos as 1,1\n");
            return false;
        }

        checkPos = game.getPionOfPlayer(1,2).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            console.log("Not same pos as 1,2\n");
            return false;
        }

        checkPos = game.getPionOfPlayer(2,1).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            console.log("Not same pos as 2,1");
            return false;
        }

        checkPos = game.getPionOfPlayer(2,2).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            console.log("Not same pos as 2,2\n");
            return false;
        }

        checkPos = game.getPionOfPlayer(player, pions);
        if(x > checkPos[0]+1 || checkPos[0]-1 > x){
            console.log("+1 or -1 for x\n");
            return false;
        }

        if(y > checkPos[0]+1 || checkPos[0]-1 > y){
            console.log("+1 or -1 for y\n");
            return false;
        }
        this.tmp = 0;
        return true;
    }

    checkValidityOfMove(player, pions){

        let x = game.getPionOfPlayer(player, pions).getPos()[0];
        let y = game.getPionOfPlayer(player, pions).getPos()[1];
        let floor = game.getGame(x,y).getNumberFloor();

        if(x==0 && y==0){
            let localTest = 0;
            //x=1,y=0
            if(game.pionIsInCell(1,0)){
                localTest+=1;
            }else if(game.getGame(1,0).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=0 y=1
            if(game.pionIsInCell(0,1)){
                localTest+=1;
            }else if(game.getGame(0,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=1 y=1
            if(game.pionIsInCell(1,1)){
                localTest+=1;
            }else if(game.getGame(1,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y==0){
            let localTest = 0;
            //x=3,y=0
            if(game.pionIsInCell(3,0)){
                localTest+=1;
            }else if(game.getGame(3,0).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=4 y=1
            if(game.pionIsInCell(4,1)){
                localTest+=1;
            }else if(game.getGame(4,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=3 y=1
            if(game.pionIsInCell(3,1)){
                localTest+=1;
            }else if(game.getGame(3,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==0 && y==MAX_COLONNE-1){
            let localTest = 0;
            //x=0 y=3
            if(game.pionIsInCell(0,MAX_COLONNE-2)){
                localTest+=1;
            }else if(game.getGame(0,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x=1 y=3
            if(game.pionIsInCell(1,MAX_COLONNE-2)){
                localTest+=1;
            }else if(game.getGame(1,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=1 y=4
            if(game.pionIsInCell(1,MAX_COLONNE-1)){
                localTest+=1;
            }else if(game.getGame(1,MAX_COLONNE-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y==MAX_COLONNE-1){
            let localTest = 0;
            //x=4 y=3
            if(game.pionIsInCell(4,MAX_COLONNE-2)){
                localTest+=1;
            }else if(game.getGame(4,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x=3 y=4
            if(game.pionIsInCell(3,MAX_COLONNE-1)){
                localTest+=1;
            }else if(game.getGame(3,MAX_COLONNE-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=3 y=3
            if(game.pionIsInCell(3,MAX_COLONNE-2)){
                localTest+=1;
            }else if(game.getGame(3,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(y==0 && x>=1 && x<=3){
            let localTest=0;
            //x-1
            if(game.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(game.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(game.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(game.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //y+1
            if(game.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(game.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x-1 y+1
            if(game.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(game.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x+1 y+1
            if(game.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(game.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            if (localTest < 5 && game.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (game.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y>=1 && y<=3){
            let localTest=0;
            //y-1
            if(game.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(game.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(game.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(game.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1
            if(game.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(game.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1 x-1
            if(game.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(game.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1 x-1
            if(game.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(game.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && game.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (game.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(y==MAX_COLONNE-1 && x>=1 && x<=3){
            let localTest=0;
            //x-1
            if(game.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(game.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(game.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(game.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1
            if(game.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(game.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(game.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(game.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y-1
            if(game.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(game.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && game.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (game.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(x==0 && y>=1 && y<=3){
            let localTest=0;
            //y-1
            if(game.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(game.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(game.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(game.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(game.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(game.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y+1
            if(game.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(game.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(game.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(game.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && game.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (game.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else{
            let localTest=0;
            //x-1
            if(game.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(game.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(game.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(game.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1
            if(game.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(game.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(game.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(game.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y-1
            if(game.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(game.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y+1
            if(game.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(game.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(game.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(game.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y+1
            if(game.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(game.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            if (localTest < 8 && game.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (game.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }
    }
}

/***************************************************************************
 *                                                                         *
 *                               FUNCTION                                  *
 *                                                                         *
 ***************************************************************************/


function resetGame(){
    game = new Game();
}
/**
 * I.A play.
 */
function playIa(player){
    console.log("AZE : SelectPion");
    let pion = selectPionIa(player);
    selectedPions = pion;
    currentPlayer = player;
    console.log("RTY : SelectCellIA");
    selectCellIa(pion, player);
    console.log("QWER : SelectBuild");
    selectBuildIa(pion, player);
}

/**
 * Evaluate date for an I.A
 * @param {Array} data Evaluate data for I.A
 */
function evaluate(data){
    let sizeOfDate = data.length-1; //cause' array start at 0.
    let currentTurn = sizeOfDate;

    let result = [];

    while( (currentTurn/sizeOfDate) > 0.01){

        console.debug("sizeOfData", sizeOfDate);
        console.debug("currentTurn", currentTurn);

        let win  = currentTurn/sizeOfDate;

        console.debug("win", win);

        //Result of winner.
        result.push([win , data[currentTurn-1], data[currentTurn]]);

        //Result of loser.
        //result.push([1-win, data[currentTurn-2], data[currentTurn-1]]);

        currentTurn -= 2;
    }

    return result;
}

/**
 * Write date in formated file.
 * @param {Array} data result data.
 */
function writeFormatedData(data){
    let size = data.length;
    let file = new File("./ia.out");
    
    let win;
    let lastPos;
    let newPos;

    for(let i = 0; i < size; i++){
        win     = data[i][0];
        lastPos = data[i][1];
        newPos  = data[i][2];

        file.write(win + ';' + lastPos + ';' + newPos + "\n");
    }
}

function readFormatedData(){
    const rl = readline.createInterface({
        input: fs.createReadStream("./ia.out")
    });


    rl.on('line', function(line) {
        let array = line.split(";");

        let lastPosData = array[1].split(',');
        let nextPosData = array[2].split(',');
        let winData = array[0];

        for( let i = 0; i < lastPosData.length; ++i){
            lastPosData[i] = parseInt(lastPosData[i], 10)/100;
        }

        for( let i = 0; i < nextPosData.length; ++i){
            nextPosData[i] = parseInt(nextPosData[i], 10)/100;
        }
        
        console.log("windata : " + winData);

        let tmp = {
            input : lastPosData.concat(nextPosData),
            
            output : 
                [winData]
        };
        net.train(tmp);
    })
    .on('close', function(line){
        console.log("Finish");
        play();
        testIA();
    }); 
}


/**
 * Return pions selected.
 */
function selectPionIa(player){
    let a = Math.random();
    let pion = 0;

    if (a > 0.5){
        pion = 1;
    }
    else {
        pion = 2;
    }
    let pos = game.getPionOfPlayer(player,pion).getPos();
    while(!canPlayWithPion(pos[0], pos[1])){
        a = Math.random();

        if (a > 0.5){
            pion = 1;
        }
        else {
            pion = 2;
        }
        pos = game.getPionOfPlayer(player,pion).getPos();
    }
    return pion;
}

function selectCellIa(selectedPawn, player){
    let Testx=Math.floor(Math.random() * Math.floor(3));
    let Testy=Math.floor(Math.random() * Math.floor(3));
    let x = 0;
    let y = 0;
    if(Testx<1){
        x = -1;
    }
    else if(Testx>=1 && Testx <2){
        x = 0;
    }
    else if(Testx>=2){
        x = 1;
    }

    if(Testy<1){
        y = -1;
    }
    else if(Testy>=1 && Testy <2){
        y = 0;
    }
    else if(Testy>=2){
        y = 1;
    }
    let pos = game.getPionOfPlayer(player,selectedPawn).getPos();
    let floorStart=game.getGame(pos[0],pos[1]).getNumberFloor();
    console.log("POS 666 : ");
    console.log(pos[0]+x);
    console.log(pos[1]+y);
    while(!game.checkValidityOfNewMove(player, selectedPawn, floorStart, pos[0]+x, pos[1]+y)){
        Testx=Math.floor(Math.random() * Math.floor(3));
        Testy=Math.floor(Math.random() * Math.floor(3));
        if(Testx<1){
            x =-1;
        }
        else if(Testx>=1 && Testx <2){
            x=0;
        }
        else if(Testx>=2){
            x=1;
        }
    
        if(Testy<1){
            y = -1;
        }
        else if(Testy>=1 && Testy <2){
            y = 0;
        }
        else if(Testy>=2){
            y = 1;
        }

        console.log("POS 667 : ");
        console.log(pos[0]+x);
        console.log(pos[1]+y);
    }
    movePionOfPlayer(player,selectedPawn,pos[0]+x,pos[1]+y);
}

function selectBuildIa(selectedPawn, player){
    let Testx=Math.floor(Math.random() * Math.floor(3));
    let Testy=Math.floor(Math.random() * Math.floor(3));
    let x;
    let y;
    if(Testx<1){
        x =-1;
    }
    else if(Testx>=1 && Testx <2){
        x=0;
    }
    else if(Testx>=2){
        x=1;
    }

    if(Testy<1){
        y =-1;
    }
    else if(Testy>=1 && Testy <2){
        y=0;
    }
    else if(Testy>=2){
        y=1;
    }
    let pos = game.getPionOfPlayer(player,selectedPawn).getPos();
    while(!game.increaseBuilding(pos[0]+x, pos[1]+y,player,selectedPawn)){
        Testx=Math.floor(Math.random() * Math.floor(3));
        Testy=Math.floor(Math.random() * Math.floor(3));
        if(Testx<1){
            x =-1;
        }
        else if(Testx>=1 && Testx <2){
            x=0;
        }
        else if(Testx>=2){
            x=1;
        }
    
        if(Testy<1){
            y = -1;
        }
        else if(Testy>=1 && Testy <2){
            y=0;
        }
        else if(Testy>=2){
            y=1;
        }
    }
}



 /**
 * Print the board in console.
 * @param {*} game
 */
function printGame(game){
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    for (let i = 0; i < x; ++i){
        console.log("\n");
        for(let j = 0; j < y; ++j){
            console.log(game.getGame(i,j));
        }
    }
}

function sendSerializeGame(game){
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i){
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j] = new Array(1);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor()*10;
            arrayToSend[i][j][0] += 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 2;

    return arrayToSend;
}

function sendBoard(game){
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i){
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j] = new Array(2);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor();
            arrayToSend[i][j][1] = 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    return arrayToSend;
}

function movePionOfPlayer(player, pions, x,y){
    game.movePionsOfPlayer(player, pions,x,y);
}

function canPlayWithPion(x,y){

    console.log([x,y]);


    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,1);
    }

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,2);
    }

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,1);
    }

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,2);
    }
    return false;
}

/**
 * Send a message to all player
 * @param   {String} msg   : Name of message
 * @param   {int}    value : Content of mesage
 */
function sendToAllPlayerMsg(msg, value){
    for (let key in currentID){
        currentID[key].emit(msg, value);
    }

}

/**
 * Send a message to one player.
 * @param   {String} msg          : Name of message
 * @param   {any}    value        : Content of mesage
 * @param   {int}    numOfPlayer  : Number of player (1 or 2 only)
 */
function sendMsgToPlayer(msg, value, numOfPlayer){
    console.log("sendMsgToPlayer = " + msg + " : " + value + " : " + numOfPlayer);
    console.log(player);
    console.log("sendMsgToPlayer(player[numOfPlayer]) : " + player[numOfPlayer-1]);
    currentID[player[numOfPlayer-1]].emit(msg,value);
}

/**
 * Send to all player an number who represent a pion.
 */
function sendAllPlayerHerPion(){
    console.log("Yes i send.");
    let pionsCpt = 1;
    for(let key in currentID){
        currentID[key].emit("yourPionIs", pionsCpt);
        pionsCpt++;
    }
}


const DEBUG = true;

const MAX_COLONNE = 5;
const MAX_LIGNE   = 5;
const MAX_FLOOR   = 4; // Level0 Level1 Level2 Level3 and BlockLevel.

var game = new Game();
var savePlayGame = [];
var currentID = {};
var player = [];

var express_G = require('express');
var app_G = express_G();
var numberOfGame = 1000;
//express settings to use the repertory 'public'
app_G.use(express_G.static('public'));
//Saving current state of game.

function play(){
    
    printGame(game);

    console.log("Send board test");
    console.log(sendBoard(game));

    while (numberOfGame != 0){
        //IA player 1.
        playIa(1);
        savePlayGame.push(sendSerializeGame(game));
        let checkGame = game.isGameIsFinish();
        console.log("checkGame = " + checkGame);
        if(checkGame != null){
            resetGame();
            let testEvaluate = evaluate(savePlayGame);
            writeFormatedData(testEvaluate);
            savePlayGame = [];
            numberOfGame--;
            continue;
        }
        //IA player 2.
        playIa(2);
        savePlayGame.push(sendSerializeGame(game));
        checkGame = game.isGameIsFinish();
        console.log("checkGame = " + checkGame);
        if(checkGame != null){
            resetGame();
            let testEvaluate = evaluate(savePlayGame);
            writeFormatedData(testEvaluate);
            savePlayGame = [];
            numberOfGame--;
            continue;
        } 
    }
}

/***************************************************************************
 *                                                                         *
 *                               SERVER                                    *
 *                                                                         *
 ***************************************************************************/


