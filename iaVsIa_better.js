//This file is an upgrade of iaVSia.js

/**
 * @description Load or create an I.A. Train I.A with data. Write in file.
 * 
 * @author MOR    Nicolas
 * @author NGUYEN Nicolas
 * 
 * @date 10/02/2020
 */
"use strict";

//All required.
var fs       = require('fs');
var readline = require("readline");
var b_serv   = require('./public/js/b_Serveur.js');
var brain    = require('./public/js/b_IA.js');


//Global variable.
var savePlayGame = [];
const DEBUG = false;
var selectedPions = undefined;
var currentPlayer = undefined;
var fileForIa     = brain.file;

const MAX_COLONNE = b_serv.MAX_COLONNE;
const MAX_LIGNE   = b_serv.MAX_LIGNE;
const MAX_FLOOR   = b_serv.MAX_FLOOR; // Level0 Level1 Level2 Level3 and BlockLevel.F

var game = new b_serv.Game();
var numberOfGame = 50;

//All about I.A
brain.readingFile();
startIA();

/***************************************************************************
 *                                                                         *
 *                               FUNCTION                                  *
 *                                                                         *
 ***************************************************************************/

/**
 * Reset the game.
 */
function resetGame()
{
    game = new b_serv.Game();
}

/**
 * Let's I.A play.
 */
function playIa(player)
{
    let pion = selectPionIa(player);
    selectedPions = pion;
    currentPlayer = player;
    selectCellIa(pion, player);
    selectBuildIa(pion, player);
}

/**
 * Return pions selected.
 */
function selectPionIa(player)
{
    let a = Math.random();
    let pion = 0;

    if (a > 0.5)
    {
        pion = 1;
    }
    else 
    {
        pion = 2;
    }
    let pos = game.getPionOfPlayer(player,pion).getPos();
    while(!canPlayWithPion(pos[0], pos[1]))
    {
        a = Math.random();

        if (a > 0.5)
        {
            pion = 1;
        }
        else 
        {
            pion = 2;
        }
        pos = game.getPionOfPlayer(player,pion).getPos();
    }
    return pion;
}

function selectCellIa(selectedPawn, player)
{
    let Testx=Math.floor(Math.random() * Math.floor(3));
    let Testy=Math.floor(Math.random() * Math.floor(3));
    let x = 0;
    let y = 0;
    if(Testx<1)
    {
        x = -1;
    }
    else if(Testx>=1 && Testx <2)
    {
        x = 0;
    }
    else if(Testx>=2)
    {
        x = 1;
    }

    if(Testy<1)
    {
        y = -1;
    }
    else if(Testy>=1 && Testy <2)
    {
        y = 0;
    }
    else if(Testy>=2)
    {
        y = 1;
    }
    let pos = game.getPionOfPlayer(player,selectedPawn).getPos();
    let floorStart=game.getGame(pos[0],pos[1]).getNumberFloor();
    while(!game.checkValidityOfNewMove(player, selectedPawn, floorStart, pos[0]+x, pos[1]+y))
    {
        Testx=Math.floor(Math.random() * Math.floor(3));
        Testy=Math.floor(Math.random() * Math.floor(3));
        if(Testx<1)
        {
            x =-1;
        }
        else if(Testx>=1 && Testx <2)
        {
            x=0;
        }
        else if(Testx>=2)
        {
            x=1;
        }
    
        if(Testy<1)
        {
            y = -1;
        }
        else if(Testy>=1 && Testy <2)
        {
            y = 0;
        }
        else if(Testy>=2)
        {
            y = 1;
        }



    }
    movePionOfPlayer(player,selectedPawn,pos[0]+x,pos[1]+y);
}

function selectBuildIa(selectedPawn, player)
{
    let Testx=Math.floor(Math.random() * Math.floor(3));
    let Testy=Math.floor(Math.random() * Math.floor(3));
    let x;
    let y;
    if(Testx<1)
    {
        x =-1;
    }
    else if(Testx>=1 && Testx <2)
    {
        x=0;
    }
    else if(Testx>=2)
    {
        x=1;
    }

    if(Testy<1)
    {
        y =-1;
    }
    else if(Testy>=1 && Testy <2)
    {
        y=0;
    }
    else if(Testy>=2)
    {
        y=1;
    }
    let pos = game.getPionOfPlayer(player,selectedPawn).getPos();
    while(!game.increaseBuilding(pos[0]+x, pos[1]+y,player,selectedPawn))
    {
        Testx=Math.floor(Math.random() * Math.floor(3));
        Testy=Math.floor(Math.random() * Math.floor(3));
        if(Testx<1)
        {
            x =-1;
        }
        else if(Testx>=1 && Testx <2)
        {
            x=0;
        }
        else if(Testx>=2)
        {
            x=1;
        }
    
        if(Testy<1)
        {
            y = -1;
        }
        else if(Testy>=1 && Testy <2)
        {
            y=0;
        }
        else if(Testy>=2)
        {
            y=1;
        }
    }
}

 /**
 * Print the board in console.
 * @param {*} game
 */
function printGame(game)
{
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    for (let i = 0; i < x; ++i)
    {
        console.log("\n");
        for(let j = 0; j < y; ++j)
        {
            console.log(game.getGame(i,j));
        }
    }
}

function sendBoard(game)
{
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j] = new Array(2);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor();
            arrayToSend[i][j][1] = 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    return arrayToSend;
}

/**
 * Write date in formated file.
 * @param {Array} data result data.
 */
function writeFormatedData(data)
{
    let size = data.length;
    let file = new b_serv.File("./ia.out");
    
    let win;
    let lastPos;
    let newPos;

    for(let i = 0; i < size; i++)
    {
        win     = data[i][0];
        lastPos = data[i][1];
        newPos  = data[i][2];

        file.write(win + ';' + lastPos + ';' + newPos + "\n");
    }
}

function sendSerializeGame(game)
{
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j] = new Array(1);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor()*10;
            arrayToSend[i][j][0] += 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][0] += 2;

    return arrayToSend;
}

function sendBoard(game)
{
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j] = new Array(2);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i)
    {
        for (let j = 0; j < MAX_LIGNE; ++j)
        {
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor();
            arrayToSend[i][j][1] = 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    return arrayToSend;
}

function selectAllPossiblyBuild(player, pions)
{

    let possibleBuild = [];

    let posCurrent = game.getPionOfPlayer(player, pions).getPos();
    let currentFloor = game.getGame(posCurrent[0], posCurrent[1]).getNumberFloor();


    /*
     x  y

    -1 -1
    -1  0
     0 -1
     0  1
     1  0
     1  1
     1 -1
    -1  1
    */
    //-1 -1
    if (game.canIncreaseBuilding(posCurrent[0]-1, posCurrent[1]-1, player, pions))
    {
        console.log("selectK 1 : " + [player, pions]);
        
        game.increaseBuilding(posCurrent[0]-1, posCurrent[1]-1, player, pions);
        possibleBuild[0] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0]-1, posCurrent[1]-1);
    } 
    
    //-1 0
    if (game.canIncreaseBuilding(posCurrent[0]-1, posCurrent[1], player, pions))
    {
        console.log("selectK 2 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0]-1, posCurrent[1], player, pions);
        possibleBuild[1] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0]-1, posCurrent[1]);
    }
    
    //0 -1
    if (game.canIncreaseBuilding(posCurrent[0], posCurrent[1]-1, player, pions))
    {
        console.log("selectK 3 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0], posCurrent[1]-1, player, pions);
        possibleBuild[2] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0], posCurrent[1]-1);
    }

    //0 1
    if (game.canIncreaseBuilding(posCurrent[0], posCurrent[1]+1, player, pions))
    {
        console.log("selectK 4 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0], posCurrent[1]+1, player, pions);
        possibleBuild[3] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0], posCurrent[1]+1);
    }

    //1 0
    if (game.canIncreaseBuilding(posCurrent[0]+1, posCurrent[1], player, pions))
    {
        console.log("selectK 5 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0]+1, posCurrent[1], player, pions);
        possibleBuild[4] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0]+1, posCurrent[1]);
    }

    //1 1
    if (game.canIncreaseBuilding(posCurrent[0]+1, posCurrent[1]+1, player, pions))
    {
        console.log("selectK 6 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0]+1, posCurrent[1]+1, player, pions);
        possibleBuild[5] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0]+1, posCurrent[1]+1);
    }

    //1 -1
    if (game.canIncreaseBuilding(posCurrent[0]+1, posCurrent[1]-1 ,player, pions))
    {
        console.log("selectK 7 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0]+1, posCurrent[1]-1, player, pions);
        possibleBuild[6] = sendSerializeGame(game);
        console.log([posCurrent]);
        game.decreaseBuilding(posCurrent[0]+1, posCurrent[1]-1);
    }
    //-1 1
    if (game.canIncreaseBuilding(posCurrent[0]-1, posCurrent[1]+1, player, pions))
    {
        console.log("selectK 8 : " + [player, pions]);

        game.increaseBuilding(posCurrent[0]-1, posCurrent[1]+1, player, pions);
        possibleBuild[7] = sendSerializeGame(game);
        game.decreaseBuilding(posCurrent[0]-1, posCurrent[1]+1);
    }

    console.log("Build");
    console.log(possibleBuild);
}

function selectAllPossiblyMove(player, pions)
{
    let possibleMove = [];

    let posCurrent = game.getPionOfPlayer(player, pions).getPos();
    let currentFloor = game.getGame(posCurrent[0], posCurrent[1]).getNumberFloor();

    /*
     x  y

    -1 -1
    -1  0
     0 -1
     0  1
     1  0
     1  1
     1 -1
    -1  1
    */
    //-1 -1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]-1, posCurrent[1]-1))
    {
        console.log("selectAll 1 : " + [player, pions]);

        game.movePionsOfPlayer(player, pions, posCurrent[0]-1, posCurrent[1]-1);
        selectAllPossiblyBuild(player, pions);
        possibleMove[0] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    } 
    
    //-1 0
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]-1, posCurrent[1]))
    {
        console.log("selectAll 2 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0]-1, posCurrent[1]);
        selectAllPossiblyBuild(player, pions);
        possibleMove[1] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }
    
    //0 -1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0], posCurrent[1]-1))
    {
        console.log("selectAll 3 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]-1);
        selectAllPossiblyBuild(player, pions);
        possibleMove[2] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }

    //0 1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0], posCurrent[1]+1))
    {
        console.log("selectAll 4 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]+1);
        selectAllPossiblyBuild(player, pions);  
        possibleMove[3] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }

    //1 0
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]+1, posCurrent[1]))
    {
        console.log("selectAll 5 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0]+1, posCurrent[1]);
        selectAllPossiblyBuild(player, pions);
        possibleMove[4] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }

    //1 1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]+1, posCurrent[1]+1))
    {
        console.log("selectAll 6 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0]+1, posCurrent[1]+1);
        selectAllPossiblyBuild(player, pions);
        possibleMove[5] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }

    //1 -1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]+1, posCurrent[1]-1))
    {
        console.log("selectAll 7 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0]+1, posCurrent[1]-1);
        selectAllPossiblyBuild(player, pions);
        possibleMove[6] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }
    //-1 1
    if (game.checkValidityOfNewMove(player, pions, currentFloor, posCurrent[0]-1, posCurrent[1]))
    {
        console.log("selectAll 8 : " + [player, pions]);
        
        game.movePionsOfPlayer(player, pions, posCurrent[0]-1, posCurrent[1]);
        selectAllPossiblyBuild(player, pions);
        possibleMove[7] = sendSerializeGame(game);
        game.movePionsOfPlayer(player, pions, posCurrent[0], posCurrent[1]);
    }

    console.log("selectAllPossiblyMove");
    console.log(posCurrent);
    console.log(possibleMove);
}

function movePionOfPlayer(player, pions, x,y)
{
    game.movePionsOfPlayer(player, pions,x,y);
}

function play()
{
    
    //printGame(game);
    //console.log("Send board test");
    //console.log(sendBoard(game));

    while (numberOfGame != 0)
    {
        selectAllPossiblyMove(1,1);
        //IA player 1.
        playIa(1);
        savePlayGame.push(sendSerializeGame(game));
        let checkGame = game.isGameIsFinish();
        //console.log("checkGame = " + checkGame);
        if(checkGame != null)
        {
            resetGame();
            let testEvaluate = evaluate(savePlayGame);
            b_serv.writeFormatedDataSync(testEvaluate, "./ia.out");
            savePlayGame = [];
            numberOfGame--;
            continue;
        }
        //IA player 2.
        playIa(2);
        savePlayGame.push(sendSerializeGame(game));
        checkGame = game.isGameIsFinish();
        //console.log("checkGame = " + checkGame);
        if(checkGame != null)
        {
            resetGame();
            let testEvaluate = evaluate(savePlayGame);
            b_serv.writeFormatedDataSync(testEvaluate, "./ia.out");
            savePlayGame = [];
            numberOfGame--;
            continue;
        } 
    }

    
}

function canPlayWithPion(x,y)
{
    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    if(x == coor[0] && y == coor[1])
    {
        return game.checkValidityOfMove(1,1);
    }

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1])
    {
        return game.checkValidityOfMove(1,2);
    }

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1])
    {
        return game.checkValidityOfMove(2,1);
    }

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1])
    {
        return game.checkValidityOfMove(2,2);
    }
    return false;
}


/**
 * Evaluate date for an I.A
 * @param {Array} data Evaluate data for I.A
 */
function evaluate(data)
{
    let sizeOfDate = data.length-1; //cause' array start at 0.
    let currentTurn = sizeOfDate;

    let result = [];

    while((currentTurn/sizeOfDate) > 0.001)
    {

        console.debug("sizeOfData", sizeOfDate);
        console.debug("currentTurn", currentTurn);

        let win  = currentTurn/sizeOfDate;

        console.debug("win", win);

        //Result of winner.
        result.push([win , data[currentTurn-1], data[currentTurn]]);

        //Result of loser.
        //result.push([1-win, data[currentTurn-2], data[currentTurn-1]]);

        currentTurn -= 2;
    }

    return result;
}

/**
 * Write date in formated file.
 * @param {Array} data result data.
 */
function writeFormatedDataSync(data)
{
    let size = data.length;
    let file = new b_serv.File("./ia.out");
    
    let win     = null;
    let lastPos = null;
    let newPos  = null;

    for(let i = 0; i < size; i++)
    {
        win     = data[i][0];
        lastPos = data[i][1];
        newPos  = data[i][2];

        file.writeSync(win + ';' + lastPos + ';' + newPos + "\n");
    }
}


function startIA(){
    play();
    brain.readFormatedDataSync("./ia.out");
    play();
    brain.readingFile('preTrain.json');
    brain.testIASantorini();
}
