/**
 * @author MOR    Nicolas
 * @author NGUYEN Nicolas
 */

"use strict";
var fs       = require('fs');
var readline = require("readline");

var b_serv = require("./public/js/b_Serveur.js");

var resultOfRead;
readFormatedData();

/***************************************************************************
 *                                                                         *
 *                               SERVER                                    *
 *                                                                         *
 ***************************************************************************/

const DEBUG = false;

const MAX_COLONNE = b_serv.MAX_COLONNE;
const MAX_LIGNE   = b_serv.MAX_LIGNE;

var game = new b_serv.Game();

var savePlayGame = [];

console.log("Server : Is starting...");
if (DEBUG)
{
    printGame(game);
}

var currentID = {};
var player = [];
var etats_G = 0;
var playerSelectedPion_G = undefined;
var pionsSelected_G      = undefined;
var pionsSelectedX_G     = undefined;
var pionsSelectedY_G     = undefined;

var express_G = require('express');
var app_G = express_G();

var server_G = app_G.listen(8080, function() {
    console.log("Server : On on port 8080 !");
});
//listener on socket
var io_G = require('socket.io').listen(server_G);
//express settings to use the repertory 'public'
app_G.use(express_G.static('public'));
// set up to
app_G.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/santorini.html');
});

io_G.on('connection', function (socket) {
    if (DEBUG)
    {
        console.debug(resultOfRead);
    }

    socket.on("login", function(id){
        console.log("Server(login) : %s is login", id);

        currentID[id] = socket;

        let size = 0;
        for (let key in currentID){
            size++;
        }
        if (size < 2){
            player.push(id);
        }
        socket.emit("login");

        //Change for number of player.
        if(size == 1){
            player.push(id);
            sendAllPlayerHerPion();
            sendToAllPlayerMsg("startGame", sendBoard(game));
            etats_G = 0;
            etats_G++;
            //socket.emit("startGame", sendBoard(game));
            sendMsgToPlayer("selectPion", null, 1);
            //currentID[id[0]].emit("selectPion");
        }
    });

    socket.on("cleanBoard", function(e){
        sendToAllPlayerMsg("cleanBoard", sendBoard(game));
    });


    socket.on("selectPion", function(coord){
        let pions = game.getPionOfPlayer(1,1);
        let coor = pions.getPos();
        if(coor[0] == coord[0] && coor[1] == coord[1]){
            playerSelectedPion_G = 1;
            pionsSelected_G      = 1;
            pionsSelectedX_G      = coord[0];
            pionsSelectedY_G      = coord[1];
        }

        pions = game.getPionOfPlayer(1,2);
        coor = pions.getPos();
        if(coor[0] == coord[0] && coor[1] == coord[1]){
            playerSelectedPion_G = 1;
            pionsSelected_G      = 2;
            pionsSelectedX_G      = coord[0];
            pionsSelectedY_G      = coord[1];
        }

        pions = game.getPionOfPlayer(2,1);
        coor = pions.getPos();
        if(coor[0] == coord[0] && coor[1] == coord[1]){
            playerSelectedPion_G = 2;
            pionsSelected_G      = 1;
            pionsSelectedX_G      = coord[0];
            pionsSelectedY_G      = coord[1];
        }

        pions = game.getPionOfPlayer(2,2);
        coor = pions.getPos();
        if(coor[0] == coord[0] && coor[1] == coord[1]){
            playerSelectedPion_G = 2;
            pionsSelected_G      = 2;
            pionsSelectedX_G      = coord[0];
            pionsSelectedY_G      = coord[1];
        }

        if (canPlayWithPion(coord[0],coord[1])){
            if(etats_G == 1){
                sendMsgToPlayer("selectPionSuccess", null, 1);
                etats_G++;
            }
            if (etats_G == 4){
                sendMsgToPlayer("selectPionSuccess", null, 2);
                etats_G++;
            }

        }
        else {
            if(etats_G == 1){
                sendMsgToPlayer("selectPionFailed", null, 1);
            }
            if (etats_G == 4){
                sendMsgToPlayer("selectPionFailed", null, 2);
            }
        }
    });

    socket.on("selectCell", function(cellCoord){
        if (game.movePionsOfPlayer(playerSelectedPion_G,
                                    pionsSelected_G,
                                    cellCoord[0],
                                    cellCoord[1]))
        {
            let pos = game.getPionOfPlayer(playerSelectedPion_G, pionsSelected_G).getPos();
            if (etats_G == 2){
                sendMsgToPlayer("selectCellSuccess", {currentX : pos[0], currentY : pos[1], newX : cellCoord[0], newY : cellCoord[1]},1);
                etats_G++;
            }
            if (etats_G == 5){
                sendMsgToPlayer("selectCellSuccess", {currentX : pos[0], currentY : pos[1], newX : cellCoord[0], newY : cellCoord[1]},2);
                etats_G++;
            }
        }
        let checkGame = game.isGameIsFinish();
        if(checkGame != null){
            sendToAllPlayerMsg("winCondition", checkGame);
            return;
        }
        else {
            if(etats_G == 2 ){
                sendMsgToPlayer("selectCellFailed", null, 1);
            }
            if(etats_G == 5){
                sendMsgToPlayer("selectCellFailed", null, 2);
            }
        }
    });

    socket.on("buildOnCell", function(coord){
        if (game.increaseBuilding(coord[0], coord[1],playerSelectedPion_G,pionsSelected_G)){
            if(etats_G == 3){
                sendMsgToPlayer("buildIncreaseSuccess", null, 1);
                
                //Saving current state of game.
                savePlayGame.push(sendBoard(game));
                
                b_serv.playIa(game);
                let checkGame = game.isGameIsFinish();
                if(checkGame != null){
                    sendToAllPlayerMsg("winCondition", checkGame);

                    let testEvaluate = b_serv.evaluate(savePlayGame);
                    b_serv.writeFormatedData(testEvaluate);

                    return;
                }
                /*sendMsgToPlayer("selectPion", null, 2);*/
                etats_G=1;
                sendMsgToPlayer("selectPion",null,1);
            }
            if (etats_G == 6){
                sendMsgToPlayer("buildIncreaseSuccess", null, 2);
                sendMsgToPlayer("selectPion", null, 1);
                etats_G = 1;
            }
            sendToAllPlayerMsg("cleanBoard", sendBoard(game));
        }
        else{
            if (etats_G == 3){
                sendMsgToPlayer("buildIncreaseFailed", null, 1);
            }
            if (etats_G == 6){
                sendMsgToPlayer("buildIncreaseFailed", null, 2);
            }
        }
    });

    socket.on("finish", function(e){
    });
    
});
//End of program


/***************************************************************************
 *                                                                         *
 *                               FUNCTION                                  *
 *                                                                         *
 ***************************************************************************/

function readFormatedData(){
    console.log("Server : Reading file ia.out...")

    let result = [];

    const rl = readline.createInterface({
        input: fs.createReadStream("./ia.out")
    });


    rl.on('line', function(line) {
        let array = line.split(";");

        let tmp = {

            input : 
            {
                lastPos : array[1],
                nextPos : array[2]
            },

            output :
            {
                win : array[0]
            }
        };
        result.push(tmp);
    })
    .on('close', function(line){
        console.log("Server : Read is finished !");
        resultOfRead =  result; 
    }); 
}

 /**
 * Print the board in console.
 * @param {*} game
 */
function printGame(game){
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    for (let i = 0; i < x; ++i){
        console.log("\n");
        for(let j = 0; j < y; ++j){
            console.log(game.getGame(i,j));
        }
    }
}

function sendBoard(game){
    let x = MAX_COLONNE;
    let y = MAX_LIGNE;

    let arrayToSend = new Array(x);
    for(let i = 0; i < MAX_COLONNE; ++i){
        arrayToSend[i] = new Array(y);
    }

    for (let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j] = new Array(2);
        }
    }

    for(let i = 0; i < MAX_COLONNE; ++i){
        for (let j = 0; j < MAX_LIGNE; ++j){
            arrayToSend[i][j][0] = game.getGame(i,j).getNumberFloor();
            arrayToSend[i][j][1] = 0;
        }
    }

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 1;

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    arrayToSend[coor[0]][coor[1]][1] = 2;

    return arrayToSend;
}

function canPlayWithPion(x,y){

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,1);
    }

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,2);
    }

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,1);
    }

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,2);
    }
    return false;
}
/**
 * Send a message to all player
 * @param   {String} msg   : Name of message
 * @param   {int}    value : Content of mesage
 */
function sendToAllPlayerMsg(msg, value){
    for (let key in currentID){
        currentID[key].emit(msg, value);
    }

}

/**
 * Send a message to one player.
 * @param   {String} msg          : Name of message
 * @param   {any}    value        : Content of mesage
 * @param   {int}    numOfPlayer  : Number of player (1 or 2 only)
 */
function sendMsgToPlayer(msg, value, numOfPlayer){
    currentID[player[numOfPlayer-1]].emit(msg,value);
}

/**
 * Send to all player an number who represent a pion.
 */
function sendAllPlayerHerPion(){
    let pionsCpt = 1;
    for(let key in currentID){
        currentID[key].emit("yourPionIs", pionsCpt);
        pionsCpt++;
    }
}

