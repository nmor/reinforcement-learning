/**
 * @author MOR    Nicolas
 * @author NGUYEN Nicolas
 */

"use strict";

const DEBUG = true;

const MAX_COLONNE = 5;
const MAX_LIGNE   = 5;
const MAX_FLOOR   = 5; // Level0 Level1 Level2 Level2 Level4 and BlockLevel.

document.addEventListener("DOMContentLoaded", function(_e) {

    var etats_G = 0;
    var yourPionsIs_G  = null;

    let selector=document.querySelector("#board");
    selector.innerHTML=createBoardHTML(MAX_COLONNE, MAX_LIGNE);

    var sock_G = io.connect();
    var name = prompt("Name");
    sock_G.emit("login", name);

    sock_G.on("startGame",function(e){
        console.log("DEBUG BOARD NULL (startGame) : "+e);
        refreshBoardHTML(e);
        console.log("test variable pionIs"+yourPionsIs_G);
        document.getElementsByClassName(yourPionsIs_G)[0].addEventListener("click", function(e){
            if(etats_G!=1){
                return;
            }
            console.log(e);
            sock_G.emit("selectPion",getXandY(e.originalTarget.offsetParent));
        });
        document.getElementsByClassName(yourPionsIs_G)[1].addEventListener("click", function(e){
            if(etats_G!=1){
                return;
            }
            console.log(e);
            sock_G.emit("selectPion",getXandY(e.originalTarget.offsetParent));
        });
        etats_G=1;
    });

    sock_G.on("yourPionIs",function(numberPion){
        console.log("yourPionIs : " + numberPion);
        yourPionsIs_G="pions"+numberPion;
    });

    sock_G.on("selectPion",function(e){
        console.log("SelectPions Socket");
        if(etats_G>1){
            return;
        }
        etats_G=1;
    });

    document.getElementById("board").addEventListener("click",function(e){
        if(etats_G<2 && etats_G>3){
            return;
        }
        console.log(e);
        if(etats_G==2){

          let test = selectGoodCell(event);
          if(test == null){return;}
          console.log("Test : " + test);
            //if(e.target.nodeName=="TD"){

                sock_G.emit("selectCell",getXandY(test));
            //}
        }

        if(etats_G==3){
            let test = selectGoodCell(event);
            if(test == null){return;}
            console.log("Test : " + test);
            //if(e.target.nodeName=="TD"){
                sock_G.emit("buildOnCell",getXandY(test));
            //}
        }
    });

    sock_G.on("selectPionSuccess",function(e){
        console.log("position accepted");
        if(etats_G>2){
            return;
        }
        etats_G=2;
    });

    sock_G.on("selectPionFailed",function(e){
        alert("position refused");
    });

    sock_G.on("selectCellSuccess",function(arrayPos){
        console.log("move allowed");
        if(etats_G>3){
            return;
        }
        etats_G=3;
        sock_G.emit("cleanBoard");
    });

    sock_G.on("winCondition", function(winPlayer){
        alert("Player " + winPlayer + " win the game. Well done.");
        return;
    });

    sock_G.on("selectCellFailed",function(e){
        alert("move not allowed");
    });

    sock_G.on("buildIncreaseSuccess",function(e){
        console.log("réussite");
        etats_G = 0;
    });

    sock_G.on("buildIncreaseFailed",function(e){
        alert("fail build");
    });

    sock_G.on("cleanBoard",function(e){
        console.log("DEBUG BOARD NULL (cleanBoard) : "+e);
        refreshBoardHTML(e);
    });

    sock_G.on("refreshBoard",function(e){
        console.log("DEBUG BOARD NULL (refreshBoard) : "+e);
        refreshBoardHTML(e);
    });

    /**
     * Print an array (one dimension) in console.
     * @param {Array} array : Board to print in Console.
     * @param {int} size    : Size of board.
     */
    function printArrayConsole(array, size){
        for(let i=0; i<size; ++i){
            console.log(array[i]);
        }
    }

    /**
     * Create Array in HTML.
     */
    function createBoardHTML(row, col){
        let HTML="<table>";
        for(let i=0; i<row; ++i){
            HTML+="<tr>";
            for(let j=0; j<col; ++j){
                HTML+="<td></td>";
            }
            HTML+="</tr>";
        }
        HTML+="</table>";
        return HTML;
    }


    /**
     * Fill an array HTML with an array js.
     *
     * @param {Array} array        : Array with HTML
     * @param {String} selectorCSS : Selector to get the Array in HTML.
     * @param {*} row              : Number of row.
     * @param {*} col              : Number of col.
     */
    function fillArrayWithHTML(array,selectorCSS,row,col){
        for(let i=0; i<row; ++i){
            for(let j=0; j<col; ++j){
                let selector=selectorCSS +" > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child("+(i+1)+")"+" > td:nth-child("+(j+1)+")";
                document.querySelector(selector).innerHTML=array[i][j].makeHtml();
            }

        }
    }
    /**
     * Return the document of row and col in the Array HTML.
     * @param {*} selectorCSS
     * @param {*} row
     * @param {*} col
     */
    function selectInArrayCell(selectorCSS, row, col){
        return document.querySelector(selectorCSS + " > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child("+(col+1)+") > td:nth-child("+(row+1)+")");
    }

    /**
     * Return true if player can play false otherwise.
     */
    function canPlay(event){
        console.log("canPlay : " + event.target);
        return true;
    }

    /**
     * Return array with x and y of array click.
     * @param {*} event
     */
    function getXandY(event){
        console.log(event);

        let x,y;
        x = event.cellIndex;
        y = event.parentElement.rowIndex;

        console.log([x,y]);

        return [x,y];
    }

    function refreshBoardHTML(board){
        for(let i=0;i<MAX_COLONNE;++i){
            for(let j=0;j<MAX_LIGNE;++j){
                if((board[i][j])[0]==0){
                    selectInArrayCell("#board",i,j).innerHTML=createHTMLFromFloor(0);
                }
                else if((board[i][j])[0]==1){
                    selectInArrayCell("#board",i,j).innerHTML=createHTMLFromFloor(1);
                }
                else if((board[i][j])[0]==2){
                    selectInArrayCell("#board",i,j).innerHTML=createHTMLFromFloor(2);
                }
                else if((board[i][j])[0]==3){
                    selectInArrayCell("#board",i,j).innerHTML=createHTMLFromFloor(3);
                }
                else if((board[i][j])[0]==4){
                    selectInArrayCell("#board",i,j).innerHTML=createHTMLFromFloor(4);
                }

                if((board[i][j])[1]==1){
                    selectInArrayCell("#board",i,j).innerHTML+="<div class=\"pions1\"></div>";
                }
                else if((board[i][j])[1]==2){
                    selectInArrayCell("#board",i,j).innerHTML+="<div class=\"pions2\"></div>";
                }
            }
        }

        document.getElementsByClassName(yourPionsIs_G)[0].addEventListener("click", function(e){
            if(etats_G!=1){
                return;
            }
            console.log(e);
            sock_G.emit("selectPion",getXandY(e.originalTarget.offsetParent));
        });
        document.getElementsByClassName(yourPionsIs_G)[1].addEventListener("click", function(e){
            if(etats_G!=1){
                return;
            }
            console.log(e);
            sock_G.emit("selectPion",getXandY(e.originalTarget.offsetParent));
        });
    }

    function createHTMLFromFloor(nombreEtage){
        switch(nombreEtage){
            case 0:
                return "";
                break;
            case 1:
                return "<div class=\"etageUn\"> </div>";
                break;
            case 2:
                return "<div class=\"etageUn\"> <div class=\"etageDeux\"> </div> </div>";
                break;
            case 3:
                return "<div class=\"etageUn\"> <div class=\"etageDeux\"> <div class=\"etageTrois\"> </div> </div> </div>";
                break;
            case 4:
                return "<div class=\"etageUn\"> <div class=\"etageDeux\"> <div class=\"etageTrois\"> <div class=\"dome\"> </div> </div> </div> </div>";
                break;
            default:
                break;
        }

    }

    function selectGoodCell(event){
        if(event.target.nodeName != "DIV" && event.target.nodeName != "TD"){
          console.log("selectGoodCell : is null");
          return null;
        }
        if(event.target.nodeName == "TD"){
          console.log("selectGoodCell : is directly TD node");
          return event.originalTarget;
        }
        //For each case
        return event.srcElement.offsetParent;
        /*switch (event.srcElement.className) {
          case "etageUn":
            return event.srcElement.offsetParent;
          case "etageDeux":
            return event.srcElement.offsetParent;
          case "etageTrois":
            return event.srcElement.offsetParent;
          default:
            return null;
        }*/
    }

});
