/**
 * @author MOR    Nicolas
 * @author NGUYEN Nicolas
 */


var fs       = require('fs');

const MAX_COLONNE = 5;
const MAX_LIGNE   = 5;
const MAX_FLOOR   = 4; // Level0 Level1 Level2 Level3 and BlockLevel.

/***************************************************************************
 *                                                                         *
 *                               CLASS                                     *
 *                                                                         *
 ***************************************************************************/

/**
 * Write and Read with this ressources.
 */
class File{

    constructor(nameOfFile){
        this.nameOfFile = nameOfFile;

        fs.appendFile('./test.data', 'HelloWorld', function(err){
            if(err) return console.error(err);
        });
    }

    write(data){
        fs.appendFile(this.nameOfFile, data, function(err){
            if(err) return console.error(err);
        });
    }
}


/**
 * Represent a Player.
 */
class Player{

    constructor(namePlayer, pions1, pions2){
        this.player = namePlayer;
        this.pions = [pions1, pions2];
    }

    getPions(x){
        switch(x){
            case 1 :
                return this.pions[0];
            case 2 :
                return this.pions[1];
            default :
                undefined;
        }
    }
}

/**
 * Represent a Building.
 */
class Building {

    constructor(){
        this.maxHeight = 4;
        this.floor     = 0;
    }

    canIncreaseBuilding(){
        if (this.maxHeight == this.floor){
            return false;
        }
        return true;
    }

    increaseBuilding(){
        if (this.maxHeight == this.floor){
            return false;
        }
        this.floor++;
        return true;
    }

    decreaseBuilding(){
        this.floor--;
        return true;
    }

    getNumberFloor(){
        return this.floor;
    }
}

class Pion{

    constructor(posX, posY){
        this.x = posX;
        this.y = posY;
    }

    moveTo(posX, posY) {
        this.x = posX;
        this.y = posY;
    }

    getPos(){
        return [this.x, this.y];
    }
}

/**
 * Represent one Cell in Array.
 */
class Cell {
    constructor(){
        this.building = new Building();
        //This would be a pawn.
    }


    build(){
        return this.building.increaseBuilding();
    }

    decreaseBuilding() {
        return this.building.decreaseBuilding();
    }

    canMove(){
        if(this.building.getNumberFloor() == MAX_FLOOR){
            return true;
        }else {
            return false;
        }
    }

    getNumberFloor(){
        return this.building.getNumberFloor();
    }
}

class Game{
    constructor(){
        let x = MAX_COLONNE;
        let y = MAX_LIGNE;

        this.game = new Array(MAX_LIGNE);
        for(let i = 0; i < MAX_COLONNE; ++i){
            this[i] = new Array(MAX_COLONNE);
        }

        for (let i = 0; i < x; ++i){
            for(let j = 0; j < y; ++j){
                this[i][j] = new Cell([0,0,0,0]);
            }
        }
        //keywordNICOLASPION
        let pions1Player1 = new Pion(0,0);
        let pions2Player1 = new Pion(0,4);
        let pions1Player2 = new Pion(4,4);
        let pions2Player2 = new Pion(4,0);


        this.player1 = new Player("test",pions1Player1, pions2Player1);
        this.player2 = new Player("I.A", pions1Player2, pions2Player2);
    }

    /**
     * Return the case of x and y
     * @param {*} x Row
     * @param {*} y
     */
    getGame(x,y){
        return this[x][y];
    }

    /**
     *
     * @param {*} x
     * @param {*} y
     * @param {*} value
     */
    setGame(x,y, value){
        this[x][y] = value;
    }

    /**
     * Return null if game can continue otherwise return the winner (1 or 2)
     */
    isGameIsFinish(){

        //Player 1 is block.
        if (!this.checkValidityOfMove(1,1) && !this.checkValidityOfMove(1,2)){
            return 2;
        }

        //Player 2 is block.
        else if (!this.checkValidityOfMove(2,1) && !this.checkValidityOfMove(2,2)){
            return 1;
        }

        //Player 1 is in max floor.
        let posPions1 = this.getPionOfPlayer(1,1).getPos();
        let posPions2 = this.getPionOfPlayer(1,2).getPos();

        if (this.getGame(posPions1[0], posPions1[1]).getNumberFloor() == MAX_FLOOR-1 || this.getGame(posPions2[0], posPions2[1]).getNumberFloor() == MAX_FLOOR-1){
            return 1;
        }

        //Player 2 is in max floor.
        posPions1 = this.getPionOfPlayer(2,1).getPos();
        posPions2 = this.getPionOfPlayer(2,2).getPos();
        if (this.getGame(posPions1[0], posPions1[1]).getNumberFloor() == MAX_FLOOR-1 || this.getGame(posPions2[0], posPions2[1]).getNumberFloor() == MAX_FLOOR-1){
            return 2;
        }
        return null;
    }


    /**
     * Get the player
     * @param {*} x
     */
    getPlayer(player){
        switch (player){
            case 1 :
                return this.player1;
            case 2 :
                return this.player2;
            default :
                return undefined;
        }
    }

    pionIsInCell(x,y){
        let arrayPions = [];

        arrayPions.push(this.getPionOfPlayer(1,1).getPos());
        arrayPions.push(this.getPionOfPlayer(1,2).getPos());
        arrayPions.push(this.getPionOfPlayer(2,1).getPos());
        arrayPions.push(this.getPionOfPlayer(2,2).getPos());

        for (let i = 0; i < 4; ++i){
            let pos = arrayPions[i];
            if(pos[0] == x && pos[1] == y){
                return true;
            }
        }
        return false;
    }

    getPionOfPlayer(player, pions){
        let playerLocal = this.getPlayer(player);
        if(playerLocal == undefined){
            return;
        }
        switch (pions){
            case 1 :
                return playerLocal.getPions(1);
            case 2 :
                return playerLocal.getPions(2);
            default :
                undefined;
        }
    }
    isBuildingFloorIsCorrect(currentX, currentY, destX, destY){
        if(destX<0 || destX>MAX_LIGNE-1 || destY<0 || destY>MAX_COLONNE-1){
            return false;
        }
        if (this.pionIsInCell(destX,destY)){
            return false;
        }
        if(currentX==destX && currentY==destY){
            return false;
        }
        if(-1 <= destX-currentX && destX-currentX <= 1 && -1 <= destY-currentY && destY-currentY <= 1){
            return true;
        }

        return false;
    }

    canIncreaseBuilding(x,y,player,pions){
        console.log("Player : " + [player, pions]);
        console.log("getPionOfPlayer");
        console.log(this.getPionOfPlayer(1,1));
        let pos = this.getPionOfPlayer(player,pions).getPos();
        return this.isBuildingFloorIsCorrect(pos[0],pos[1],x,y);
    };

    decreaseBuilding(x,y){
        return this[x][y].decreaseBuilding();
    }

    increaseBuilding(x,y,player,pions){
        if(!this.canIncreaseBuilding(x,y,player,pions)){
            return false;
        }
        return this[x][y].build();
    }

    movePionsOfPlayer(player, pions,x,y){
        let pos = this.getPionOfPlayer(player, pions).getPos();
        let currentX =  pos[0];
        let currentY =  pos[1];

        let floorStart = this.getGame(currentX,currentY).getNumberFloor();

        if(-1 <= x-currentX && x-currentX <= 1 && -1 <= y-currentY && y-currentY <= 1){
            if (this.checkValidityOfNewMove(player, pions, floorStart, x,y)){
                this.getPionOfPlayer(player, pions).moveTo(x,y);
                return true;
            }
        }
        else{
            return false;
        }
    }

    /**
     *
     * @param {int} player         : 1->player1 2->player2
     * @param {int} pions          : 1->pions1  2->pions2
     * @param {int} floorStart     : stating floor
     * @param {int} x              : new X cord.
     * @param {int} y              : new Y cord.
     */
    checkValidityOfNewMove(player, pions, floorStart, x, y){
        if(x < 0 || y < 0 || x > MAX_LIGNE-1|| y > MAX_COLONNE-1){
            return false;
        }

        if (this.getGame(x,y).getNumberFloor() == MAX_FLOOR){
            return false;
        }

        if (this.getGame(x,y).getNumberFloor() > floorStart+1){
            return false;
        }

        let checkPos = this.getPionOfPlayer(1,1).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            return false;
        }

        checkPos = this.getPionOfPlayer(1,2).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            return false;
        }

        checkPos = this.getPionOfPlayer(2,1).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            return false;
        }

        checkPos = this.getPionOfPlayer(2,2).getPos();
        if(checkPos[0] == x && checkPos[1] == y){
            return false;
        }

        checkPos = this.getPionOfPlayer(player, pions);
        if(x > checkPos[0]+1 || checkPos[0]-1 > x){
            return false;
        }

        if(y > checkPos[0]+1 || checkPos[0]-1 > y){
            return false;
        }

        return true;
    }

    checkValidityOfMove(player, pions){

        let x = this.getPionOfPlayer(player, pions).getPos()[0];
        let y = this.getPionOfPlayer(player, pions).getPos()[1];
        let floor = this.getGame(x,y).getNumberFloor();

        if(x==0 && y==0){
            let localTest = 0;
            //x=1,y=0
            if(this.pionIsInCell(1,0)){
                localTest+=1;
            }else if(this.getGame(1,0).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=0 y=1
            if(this.pionIsInCell(0,1)){
                localTest+=1;
            }else if(this.getGame(0,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=1 y=1
            if(this.pionIsInCell(1,1)){
                localTest+=1;
            }else if(this.getGame(1,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y==0){
            let localTest = 0;
            //x=3,y=0
            if(this.pionIsInCell(3,0)){
                localTest+=1;
            }else if(this.getGame(3,0).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=4 y=1
            if(this.pionIsInCell(4,1)){
                localTest+=1;
            }else if(this.getGame(4,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=3 y=1
            if(this.pionIsInCell(3,1)){
                localTest+=1;
            }else if(this.getGame(3,1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==0 && y==MAX_COLONNE-1){
            let localTest = 0;
            //x=0 y=3
            if(this.pionIsInCell(0,MAX_COLONNE-2)){
                localTest+=1;
            }else if(this.getGame(0,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x=1 y=3
            if(this.pionIsInCell(1,MAX_COLONNE-2)){
                localTest+=1;
            }else if(this.getGame(1,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=1 y=4
            if(this.pionIsInCell(1,MAX_COLONNE-1)){
                localTest+=1;
            }else if(this.getGame(1,MAX_COLONNE-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y==MAX_COLONNE-1){
            let localTest = 0;
            //x=4 y=3
            if(this.pionIsInCell(4,MAX_COLONNE-2)){
                localTest+=1;
            }else if(this.getGame(4,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x=3 y=4
            if(this.pionIsInCell(3,MAX_COLONNE-1)){
                localTest+=1;
            }else if(this.getGame(3,MAX_COLONNE-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x=3 y=3
            if(this.pionIsInCell(3,MAX_COLONNE-2)){
                localTest+=1;
            }else if(this.getGame(3,MAX_COLONNE-2).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 3){
                return true;
            }
            return false;
        }

        else if(y==0 && x>=1 && x<=3){
            let localTest=0;
            //x-1
            if(this.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(this.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(this.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(this.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //y+1
            if(this.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(this.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x-1 y+1
            if(this.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(this.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            //x+1 y+1
            if(this.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(this.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            if (localTest < 5 && this.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (this.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(x==MAX_COLONNE-1 && y>=1 && y<=3){
            let localTest=0;
            //y-1
            if(this.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(this.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(this.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(this.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1
            if(this.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(this.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1 x-1
            if(this.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(this.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1 x-1
            if(this.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(this.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && this.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (this.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(y==MAX_COLONNE-1 && x>=1 && x<=3){
            let localTest=0;
            //x-1
            if(this.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(this.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(this.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(this.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1
            if(this.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(this.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(this.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(this.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y-1
            if(this.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(this.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && this.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (this.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else if(x==0 && y>=1 && y<=3){
            let localTest=0;
            //y-1
            if(this.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(this.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(this.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(this.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(this.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(this.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y+1
            if(this.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(this.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(this.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(this.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }

            if (localTest < 5 && this.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (this.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }

        else{
            let localTest=0;
            //x-1
            if(this.pionIsInCell(x-1,y)){
                localTest+=1;
            }else if(this.getGame(x-1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1
            if(this.pionIsInCell(x+1,y)){
                localTest+=1;
            }else if(this.getGame(x+1,y).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y-1
            if(this.pionIsInCell(x,y-1)){
                localTest+=1;
            }else if(this.getGame(x,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //y+1
            if(this.pionIsInCell(x,y+1)){
                localTest+=1;
            }else if(this.getGame(x,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y-1
            if(this.pionIsInCell(x-1,y-1)){
                localTest+=1;
            }else if(this.getGame(x-1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x-1 y+1
            if(this.pionIsInCell(x-1,y+1)){
                localTest+=1;
            }else if(this.getGame(x-1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y-1
            if(this.pionIsInCell(x+1,y-1)){
                localTest+=1;
            }else if(this.getGame(x+1,y-1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            //x+1 y+1
            if(this.pionIsInCell(x+1,y+1)){
                localTest+=1;
            }else if(this.getGame(x+1,y+1).getNumberFloor() - floor > 1){
                localTest+=1;
            }
            if (localTest < 8 && this.getGame(x,y).getNumberFloor != MAX_FLOOR){
                if (this.getGame(x,y).getNumberFloor() - floor <= 1){
                    return true;
                }
            }
            return false;
        }
    }
}

/***************************************************************************
 *                                                                         *
 *                               FUNCTION                                  *
 *                                                                         *
 ***************************************************************************/
/**
 * I.A play.
 */
exports.playIa = function(game){
    let pion=selectPionIa(game);
    selectCellIa(pion, game);
    selectBuildIa(pion, game);
}

/**
 * Return pions selected.
 */
function selectPionIa(game){
    let a = Math.random();
    let pion = 0;

    if (a > 0.5){
        pion = 1;
    }
    else {
        pion = 2;
    }
    let pos = game.getPionOfPlayer(2,pion).getPos();
    while(!canPlayWithPion(pos[0], pos[1], game)){
        a = Math.random();

        if (a > 0.5){
            pion = 1;
        }
        else {
            pion = 2;
        }
        pos = game.getPionOfPlayer(2,pion).getPos();
    }
    return pion;
}

function selectCellIa(selectedPawn, game){
    let Testx=Math.random()*3;
    let Testy=Math.random()*3;
    let x;
    let y;
    if(Testx<1){
        x = -1;
    }
    else if(Testx>=1 && Testx <2){
        x=0;
    }
    else if(Testx>=2){
        x=1;
    }

    if(Testy<1){
        y =-1;
    }
    else if(Testy>=1 && Testy <2){
        y=0;
    }
    else if(Testy>=2){
        y=1;
    }
    let pos = game.getPionOfPlayer(2,selectedPawn).getPos();
    let floorStart=game.getGame(pos[0],pos|[1]).getNumberFloor();
    while(!game.checkValidityOfNewMove(2, selectedPawn, floorStart, pos[0]+x, pos[1]+y)){
        Testx=Math.random()*3;
        Testy=Math.random()*3;
        if(Testx<1){
            x =-1;
        }
        else if(Testx>=1 && Testx <2){
            x=0;
        }
        else if(Testx>=2){
            x=1;
        }
    
        if(Testy<1){
            y = -1;
        }
        else if(Testy>=1 && Testy <2){
            y=0;
        }
        else if(Testy>=2){
            y=1;
        }
    }
    movePionOfPlayer(2,selectedPawn,pos[0]+x,pos[1]+y, game);
}

function selectBuildIa(selectedPawn, game){
    let Testx=Math.random()*3;
    let Testy=Math.random()*3;
    let x;
    let y;
    if(Testx<1){
        x =-1;
    }
    else if(Testx>=1 && Testx <2){
        x=0;
    }
    else if(Testx>=2){
        x=1;
    }

    if(Testy<1){
        y =-1;
    }
    else if(Testy>=1 && Testy <2){
        y=0;
    }
    else if(Testy>=2){
        y=1;
    }
    let pos = game.getPionOfPlayer(2,selectedPawn).getPos();
    while(!game.increaseBuilding(pos[0]+x, pos[1]+y,2,selectedPawn)){
        Testx=Math.random()*3;
        Testy=Math.random()*3;
        if(Testx<1){
            x =-1;
        }
        else if(Testx>=1 && Testx <2){
            x=0;
        }
        else if(Testx>=2){
            x=1;
        }
    
        if(Testy<1){
            y = -1;
        }
        else if(Testy>=1 && Testy <2){
            y=0;
        }
        else if(Testy>=2){
            y=1;
        }
    }
}

function movePionOfPlayer(player, pions, x,y, game){
    game.movePionsOfPlayer(player, pions,x,y);
}

function canPlayWithPion(x,y,game){

    let pions = game.getPionOfPlayer(1,1);
    let coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,1);
    }

    pions = game.getPionOfPlayer(1,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(1,2);
    }

    pions = game.getPionOfPlayer(2,1);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,1);
    }

    pions = game.getPionOfPlayer(2,2);
    coor = pions.getPos();
    if(x == coor[0] && y == coor[1]){
        return game.checkValidityOfMove(2,2);
    }
    return false;
}

 /**
 * Evaluate date for an I.A
 * @param {Array} data Evaluate data for I.A
 */
exports.evaluate = function(data) {
    let sizeOfDate = data.length-1; //cause' array start at 0.
    let currentTurn = sizeOfDate;

    let result = [];

    // We won't data under 50% cause' of 
    // we can't say any think about that data.
    while( (currentTurn/sizeOfDate) > 0.5){

        let win  = currentTurn/sizeOfDate;

        //Result of winner.
        result.push([win , data[currentTurn-1], data[currentTurn]]);

        //Result of loser.
        result.push([1-win, data[currentTurn-1], data[currentTurn]]);

        currentTurn -= 2;
    }

    return result;
}

/**
 * Write date in formated file.
 * @param {Array} data result data.
 */
exports.writeFormatedData = function(data) {
    let size = data.length;
    let file = new File("./ia.out");
    
    let win;
    let lastPos;
    let newPos;

    for(let i = 0; i < size; i++){
        win     = data[i][0];
        lastPos = data[i][1];
        newPos  = data[i][2];

        file.write(win + ';' + lastPos + ';' + newPos + "\n");
    }
}

/**
 * Write date in formated file.
 * @param {Array} data result data.
 */
exports.writeFormatedDataSync = function(data, file)
{
    let size = data.length;
    
    let win     = null;
    let lastPos = null;
    let newPos  = null;

    for(let i = 0; i < size; i++)
    {
        win     = data[i][0];
        lastPos = data[i][1];
        newPos  = data[i][2];

        fs.appendFileSync(file, win + ';' + lastPos + ';' + newPos + "\n");
    }
}



 /***************************************************************************
 *                                                                         *
 *                                 EXPORT                                  *
 *                                                                         *
 ***************************************************************************/

//Class
exports.Game = Game;
exports.Cell = Cell;
exports.Building = Building;
exports.Pion = Pion;
exports.Player = Player;
exports.File = File;

//Function

//Const
exports.MAX_COLONNE = MAX_COLONNE;
exports.MAX_LIGNE = MAX_LIGNE;
exports.MAX_FLOOR = MAX_FLOOR;
